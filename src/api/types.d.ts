type Quote = {
    author: string;
    id: number;
    text: string;
};
