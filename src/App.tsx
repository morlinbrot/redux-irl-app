import React from 'react';

import { GlobalStyles } from './globalStyles';

import { MyClassComp } from './compositions/MyClassComp';
import { Notifications } from './compositions/Notifications';
import { StatsHeader } from './compositions/StatsHeader';

export function App() {
  return (
    <>
      <GlobalStyles />
      <h1>React + Redux IRL App</h1>
      <p>Real life React + Redux app boilerplate, bundled with Webpack, written in Typescript</p>
      <Notifications />
      <StatsHeader />
      <MyClassComp />
    </>
  )
}