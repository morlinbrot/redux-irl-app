import React from 'react';
import { useSelector } from 'react-redux';

import { selectNotifications } from '../../store/notifications';

import { Toast } from '../../components/Toast';

import { NotificationsStyled } from './Notifications.styled';


export function Notifications() {

  const notifications = useSelector(selectNotifications);

  return (
    <NotificationsStyled>
      {notifications.map(notification => {
        return (
          <Toast
            key={notification.id}
            {...notification}
          />
        );
      })}
    </NotificationsStyled>
  )
}
