import React from 'react';

import { QuoteStyled } from './Quote.styled';

type Props = {
  author: string;
  text: string;
}

export function Quote({ author, text }: Props) {
  return (
    <QuoteStyled>
      <p className="text">{text}</p>
      <p className="author">{author}</p>
    </QuoteStyled>
  )
}