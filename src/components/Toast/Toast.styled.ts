import styled from 'styled-components';


export const ToastStyled = styled.div`
  min-width: 250px;
  border: 1px solid gray;
  padding: 10px;

  header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    .btn-close {
      right: 0;
    }
  }
`;