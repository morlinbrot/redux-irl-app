import { configureStore, Action } from '@reduxjs/toolkit';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import reduxIrl from 'redux-irl';

import { actions as notificationActions } from './notifications';
import { rootReducer, RootAppState } from './rootReducer';
import { useDispatch } from 'react-redux';


export const store = configureStore({
    reducer: rootReducer,
});


reduxIrl.init({
    defaultProcessor: (res: any) => {
        return res;
    },
    errorHandler: (err) => {
        store.dispatch({
            ...notificationActions.open({
                id: 'unique-id',
                message: err.message,
                title: err.name,
                type: 'error',
            }),
        });
    }
});


if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./rootReducer', () => {
        const newRootReducer = require('./rootReducer').default;
        store.replaceReducer(newRootReducer);
    });
}


// ThunkAction<1. Return value, 2. State for getState, 3. "Extra argument", 4. Action types accepted by `dispatch`>
export type AppThunkAction = ThunkAction<void, RootAppState, null, Action<string>>;
export type ReduxDispatch = ThunkDispatch<typeof store.dispatch, any, Action>;

// Typed `useDispatch` for `.then(doSomething)` to work on `dispatch(myThunkActionCreator())` in Functional Components.
export function useReduxDispatch(): ReduxDispatch {
    return useDispatch<ReduxDispatch>();
}
