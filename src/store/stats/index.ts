import {
  DataState,
  Draft,
  FsaPayloadAction,
  makeSlice,
} from 'redux-irl';

import { actions as quotesActions } from '../quotes';
import { RootAppState } from '../rootReducer';


type Stats = {
  fetchCount: number;
}

const initialStatsStore = { fetchCount: 0 };

const fetchCountReducer = (state: Draft<DataState<Stats>>, action: FsaPayloadAction) => {
  state.data.fetchCount += 1;
}
const extraReducers = {
  [quotesActions.setPending.type]: fetchCountReducer,
}

const statsSlice = makeSlice('stats', initialStatsStore, extraReducers);


export function statsSelector(state: RootAppState) {
  return state.stats.data;
}

export const actions = statsSlice.actions;
export const stats = statsSlice.reducer;
