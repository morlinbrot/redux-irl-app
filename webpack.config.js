const path = require('path');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devServer: {
    hot: true,
  },
  devtool: 'source-map',
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [{
          loader: 'ts-loader',
          options: { transpileOnly: true },
        }],
      },
    ]
  },
  output: {
    filename: 'bundle.js',
  },
  plugins: [
    // Clean the dist folder before a build starts.
    new CleanWebpackPlugin(),
    // Speed up compilation by running type checking on another thread.
    new ForkTsCheckerWebpackPlugin(),
    // Generate an output HTML file and insert scripts automatically.
    new HtmlWebpackPlugin({ template: './index.html' }),
    // Hot Module Replacment.
    new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    extensions: ['*', '.js', '.jsx', '.ts', '.tsx']
  },
};
